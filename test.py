from github import Github
import os
from rich import print

token = os.getenv('GITHUB_TOKEN')
g = Github(token)
repo = g.get_repo("willmcgugan/rich")
issues = repo.get_issues(state="open")
print(issues.get_page(0))